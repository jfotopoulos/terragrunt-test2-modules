resource "aws_s3_bucket" "sandbox-workable-profile" {
  bucket = "sandbox-workable-profile"
  acl    = "private"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PublicReadAccess",
      "Action": [
        "s3:GetObject"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::sandbox-workable-profile/*"
      ],
      "Principal": "*"
    }
  ]
}
EOF

  tags {
    Environment = "sandbox"
    Provisioner = "Terraform"
    Owner       = "fotopoulos@workable.com"
    JIRA        = ""
  }
}

resource "aws_s3_bucket" "jf_microservice2_stg33" {
  bucket        = "jf-microservice2-stg33"
  region        = "${var.aws_region}"
  acl           = "private"
  force_destroy = "true"

  tags {
    Environment = "sandbox"
    Owner       = "JF"
    Provisioner = "Terraform"
  }
}
