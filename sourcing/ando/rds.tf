resource "aws_db_instance" "ando_db" {
  allocated_storage      = "${var.rds_disk_size}"
  storage_type           = "${var.rds_disk_type}"
  skip_final_snapshot    = true
  engine                 = "postgres"
  engine_version         = "9.6.8"
  multi_az               = "${var.is_this_multi_az}"
  port                   = "${var.db_port}"
  instance_class         = "${var.db_instance_type}"
  name                   = "${var.primary_db_name}"
  username               = "${var.db_administrator_user}"
  password               = "${var.db_administrator_passwd}"
  parameter_group_name   = "${aws_db_parameter_group.standard-prod-postgres9-6-paramgroup.id}"
  publicly_accessible    = "true"
  vpc_security_group_ids = ["${aws_security_group.db_ando_sg.id}"]

  tags = {
    "Name"        = "${var.db_instance_name}"
    "Provisioner" = "Terraform"
    "Owner"       = "${var.rds_owner_email}"
  }
}
