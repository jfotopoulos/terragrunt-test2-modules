# environment variables
variable "env_name" {
  description = "The name of this specific environment"
  default     = "staging"
}

# s3 related
variable "bucket_name" {
  description = "Specify the name of the S3 bucket"
}

variable "bucket2_name" {
  description = "Specify the name of the second S3 bucket"
}

variable "owner_email" {
  description = "The email of the owner of this S3 bucket"
  default     = "fotopoulos@workable.com"
}

variable "bucket_count" {
  description = "How many S3 buckets should be created"
  default     = 1
}

# rds param group related vars
variable "pg_application_name" {
  description = "The pg_application_name parameter"
  default     = "psql non-interactive"
}

variable "pg_autovacuum" {
  description = "pg_autovacuum parameter"
  default     = 1
}

variable "pg_lc_messages" {
  description = "pg_lc_messages parameter"
  default     = "en_US.UTF-8"
}

# RDS instance variables
variable "rds_disk_size" {
  description = "The disk size of the rds instance"
  default     = 10
}

variable "rds_disk_type" {
  description = "The RDS disk type that will be used"
  default     = "gp2"
}

variable "db_instance_name" {
  description = "The name of the RDS instance that Terraform will use."
}

variable "is_this_multi_az" {
  description = "Whether this is a multi AZ instance of not (boolean)"
  default     = false
}

variable "db_port" {
  description = "The database port"
  default     = 5432
}

variable "db_instance_type" {
  description = "The instance type of this instance"
  default     = "db.m3.medium"
}

variable "primary_db_name" {
  description = "The name of the database that will be hosted on this instance"
}

variable "db_administrator_user" {
  description = "database administrator user"
  default     = "master"
}

variable "db_administrator_passwd" {
  description = "The administrator password"
}

variable "rds_owner_email" {
  description = "This describes the real life user that created/requested this database"
}
