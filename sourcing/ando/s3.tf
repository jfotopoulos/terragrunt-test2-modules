resource "aws_s3_bucket" "terragrunt_s3_bucket" {
  count  = "${var.bucket_count}"
  bucket = "${var.bucket_name}-${count.index}"
  acl    = "private"

  tags {
    Name     = "${var.bucket_name}"
    Provider = "Terraform"
    Owner    = "${var.owner_email}"
  }
}

resource "aws_s3_bucket_policy" "terragrunt_s3_bucket_policy" {
  count  = "${var.bucket_count > 0 ? 1 : 0}"
  bucket = "${var.bucket_name}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::156460612806:root"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::${var.bucket_name}*/*/AWSLogs/674549574953/*"
        }
    ]
}
POLICY
}

# resource "aws_s3_bucket" "terragrunt_s3_bucket2" {
#   bucket = "${var.bucket2_name}"
#   acl    = "private"


#   tags {
#     Name     = "${var.bucket2_name}"
#     Provider = "Terraform"
#     Owner    = "${var.owner_email}"
#   }
# }

