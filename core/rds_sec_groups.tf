resource "aws_security_group" "db_core_sg" {
  name        = "${var.env_name}_db_core_sg"
  description = "Allow all incoming traffic on postgres port"

  ingress {
    from_port   = "${var.db_port}"
    to_port     = "${var.db_port}"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name        = "${var.env_name}_db_core_sg"
    Provisioner = "Terraform"
    Owner       = "${var.rds_owner_email}"
    Environment = "${var.env_name}"
  }
}

output "core_db_sec_group_id" {
  value = "${aws_security_group.db_core_sg.id}"
}
