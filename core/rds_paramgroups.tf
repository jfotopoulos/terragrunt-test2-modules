resource "aws_db_parameter_group" "workable-core-pg96-paramgroup" {
  name   = "${var.env_name}-${var.db_param_group_name}"
  family = "postgres9.6"

  parameter {
    name         = "application_name"
    value        = "${var.pg_application_name}"
    apply_method = "immediate"
  }

  parameter {
    name         = "autovacuum"
    value        = "${var.pg_autovacuum}"
    apply_method = "immediate"
  }

  parameter {
    name         = "lc_messages"
    value        = "${var.pg_lc_messages}"
    apply_method = "immediate"
  }

  parameter {
    name         = "lc_monetary"
    value        = "en_US.UTF-8"
    apply_method = "immediate"
  }

  parameter {
    name         = "lc_numeric"
    value        = "en_US.UTF-8"
    apply_method = "immediate"
  }

  parameter {
    name         = "lc_time"
    value        = "en_US.UTF-8"
    apply_method = "immediate"
  }

  parameter {
    name         = "log_autovacuum_min_duration"
    value        = 30000
    apply_method = "immediate"
  }

  parameter {
    name         = "log_connections"
    value        = 1
    apply_method = "immediate"
  }

  parameter {
    name         = "log_lock_waits"
    value        = true
    apply_method = "immediate"
  }

  parameter {
    name         = "log_min_duration_statement"
    value        = 2000
    apply_method = "immediate"
  }

  parameter {
    name         = "log_temp_files"
    value        = 10240
    apply_method = "immediate"
  }

  parameter {
    name         = "max_parallel_workers_per_gather"
    value        = 3
    apply_method = "immediate"
  }

  parameter {
    name         = "max_standby_archive_delay"
    value        = 36000000
    apply_method = "immediate"
  }

  parameter {
    name         = "max_standby_streaming_delay"
    value        = 36000000
    apply_method = "immediate"
  }

  parameter {
    name         = "max_replication_slots"
    value        = 20
    apply_method = "pending-reboot"
  }

  parameter {
    name         = "max_worker_processes"
    value        = 20
    apply_method = "pending-reboot"
  }

  parameter {
    name         = "rds.force_ssl"
    value        = false
    apply_method = "pending-reboot"
  }

  # this is the maximum allowed
  parameter {
    name         = "maintenance_work_mem"
    value        = 2147483647
    apply_method = "immediate"
  }

  parameter {
    name         = "max_connections"
    value        = 500
    apply_method = "pending-reboot"
  }

  parameter {
    name         = "wal_keep_segments"
    value        = 64
    apply_method = "immediate"
  }

  parameter {
    name         = "work_mem"
    value        = 122000000
    apply_method = "immediate"
  }

  parameter {
    name         = "shared_preload_libraries"
    value        = "pg_stat_statements"
    apply_method = "pending-reboot"
  }

  tags {
    Name        = "${var.env_name}_${var.db_param_group_name}"
    Environment = "${var.env_name}"
    Provisioner = "Terraform"
    Owner       = "${var.rds_owner_email}"
  }
}

output "workable-core-pg96-paramgroup-id" {
  value = "${aws_db_parameter_group.workable-core-pg96-paramgroup.id}"
}
