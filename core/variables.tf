# environment variables
variable "env_name" {
  description = "The name of this specific environment"
}

# rds param group related vars
variable "parameter_group_family" {
  description = "The family of this parameter group"
  default     = "postgres9.6"
}

variable "db_param_group_name" {
  description = "The name of this parameter group"
  default     = "workable-core-pg96-paramgroup"
}

variable "pg_application_name" {
  description = "The pg_application_name parameter"
  default     = "psql non-interactive"
}

variable "pg_autovacuum" {
  description = "pg_autovacuum parameter"
  default     = 1
}

variable "pg_lc_messages" {
  description = "pg_lc_messages parameter"
  default     = "en_US.UTF-8"
}

# RDS instance variables
variable "rds_disk_size" {
  description = "The disk size of the rds instance"
  default     = 10
}

variable "rds_disk_type" {
  description = "The RDS disk type that will be used"
  default     = "gp2"
}

variable "db_instance_name" {
  description = "The name of the RDS instance that Terraform will use."
}

variable "is_this_multi_az" {
  description = "Whether this is a multi AZ instance of not (boolean)"
  default     = false
}

variable "db_port" {
  description = "The database port"
  default     = 5432
}

variable "db_instance_type" {
  description = "The instance type of this instance"
  default     = "db.m3.medium"
}

variable "primary_db_name" {
  description = "The name of the database that will be hosted on this instance"
}

variable "db_administrator_user" {
  description = "database administrator user"
  default     = "master"
}

variable "db_administrator_passwd" {
  description = "The administrator password"
}

variable "rds_owner_email" {
  description = "This describes the real life user that created/requested this database"
}
