data "terraform_remote_state" "core_db_sec_group" {
  backend = "s3"

  config {
    region  = "${var.core_db_region}"
    bucket  = "${var.core_db_statefile_bucket}"
    key     = "${var.core_db_statefile_path}/terraform.tfstate"
    profile = "${var.core_db_backend_profile}"
  }
}

resource "aws_db_instance" "metricsdb" {
  allocated_storage      = "${var.rds_disk_size}"
  storage_type           = "${var.rds_disk_type}"
  skip_final_snapshot    = true
  engine                 = "postgres"
  engine_version         = "9.6.9"
  multi_az               = "${var.is_this_multi_az}"
  port                   = "${var.db_port}"
  instance_class         = "${var.db_instance_type}"
  name                   = "${var.primary_db_name}"
  username               = "${var.db_administrator_user}"
  password               = "${var.db_administrator_passwd}"
  parameter_group_name   = "${aws_db_parameter_group.metricsdb-pg96-paramgroup.id}"
  publicly_accessible    = "true"
  vpc_security_group_ids = ["${data.terraform_remote_state.core_db_sec_group.core_db_sec_group_id}"]

  tags = {
    "Name"        = "${var.db_instance_name}"
    "Provisioner" = "Terraform"
    "Owner"       = "${var.rds_owner_email}"
    "Environment" = "${var.env_name}"
  }
}

output "rds_instance_endpoint" {
  value = "${aws_db_instance.metricsdb.endpoint}"
}

output "rds_instance_id" {
  value = "${aws_db_instance.metricsdb.id}"
}

output "rds_instance_arn" {
  value = "${aws_db_instance.metricsdb.arn}"
}

output "rds_instance_hostname" {
  value = "${aws_db_instance.metricsdb.address}"
}
