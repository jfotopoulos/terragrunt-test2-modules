# variable "bucket_name" {
#   description = "wilson specific s3 bucket name"
#   default     = "wilson-s3-bucket-for-testing-terragrunt"
# }

# variable "bucket_count" {
#   description = "how many of these wilson buckets should be created"
#   default     = 1
# }

variable "owner_email" {
	default     = "sre@workable.com"
}

variable "bucket_names" {
	type = "list"
	description = "A list with bucket names for wilson microservice"
	default = []
}