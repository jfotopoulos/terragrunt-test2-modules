variable "aws_region" {
  default = "us-east-2"
}

variable "aws_profile" {
  description = "The AWS profile to use for this account"
}

variable "google_project_id" {
  description = "The ID of our google project"
}

variable "google_creds" {
  description = "This should be the name of a json file that is located in ./ so it can be easily accessible. "
  default     = "./gcp-account.json"
}

variable "google_region" {
  description = "The region of google that will be used."
  default     = "us-central1"
}

provider "aws" {
  version = "~> 1.5"
  region  = "${var.aws_region}"
  profile = "${var.aws_profile}"
}

provider "google" {
  credentials = "${file("${var.google_creds}")}"
  project = "${var.google_project_id}"
  region = "${var.google_region}"
}

# Get the backend from parent folder
terraform {
  backend "s3" {}
}
