resource "aws_s3_bucket" "wilson_s3_buckets" {
  count  = "${count(var.bucket_names)}"
  #bucket = "${var.bucket_name}-${count.index}"
  bucket = "${element(var.bucket_names, count.index)}"
  acl    = "private"

  tags {
    Name     = "${element(var.bucket_names, count.index)}"
    Provider = "Terraform"
    Owner    = "${var.owner_email}"
  }
}
