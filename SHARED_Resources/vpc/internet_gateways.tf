resource "aws_internet_gateway" "usv-igw-sandbox-test" {
  vpc_id = "${aws_vpc.usv-vpc-sandbox-test.id}"

  tags {
    Name        = "${var.igw_name}"
    Owner       = "${var.owner_email}"
    Provisioner = "Terraform"
    Environment = "${var.vpc_environment}"
  }
}

output "igw_id" {
  value = "${aws_internet_gateway.usv-igw-sandbox-test.id}"
}
