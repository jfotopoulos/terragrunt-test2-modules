resource "aws_route" "primary_routes" {
  count                  = "${length(var.vpc_routes_with_primary_default_gw)}"
  route_table_id         = "${aws_route_table.primary_route_table.id}"
  gateway_id             = "${aws_internet_gateway.usv-igw-sandbox-test.id}"
  destination_cidr_block = "${element(var.vpc_routes_with_primary_default_gw, count.index)}"
}

output "route_destinations" {
  value = "${aws_route.primary_routes.*.destination_cidr_block}"
}
