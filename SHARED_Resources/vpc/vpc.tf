resource "aws_vpc" "usv-vpc-sandbox-test" {
  cidr_block           = "${var.vpc_cidr}"
  enable_dns_hostnames = "${var.enable_DNS_names}"
  enable_dns_support   = "${var.enable_DNS_support}"
  instance_tenancy     = "${var.tenancy}"

  tags {
    Name        = "${var.vpc_name}"
    Owner       = "${var.owner_email}"
    Provisioner = "Terraform"
    Environment = "${var.vpc_environment}"
  }
}

output "vpc_arn" {
  value = "${aws_vpc.usv-vpc-sandbox-test.arn}"
}

output "vpc_id" {
  value = "${aws_vpc.usv-vpc-sandbox-test.id}"
}

output "vpc_cidr" {
  value = "${aws_vpc.usv-vpc-sandbox-test.cidr_block}"
}

output "vpc_default_sg" {
  value = "${aws_vpc.usv-vpc-sandbox-test.default_security_group_id}"
}

output "vpc_default_route_table" {
  value = "${aws_vpc.usv-vpc-sandbox-test.default_route_table_id}"
}

output "vpc_main_route_table" {
  value = "${aws_vpc.usv-vpc-sandbox-test.main_route_table_id}"
}
