data "aws_availability_zones" "available" {}

resource "aws_subnet" "usv-subnets-sandbox-test" {
  count = "${var.subnet_count}"

  vpc_id                  = "${aws_vpc.usv-vpc-sandbox-test.id}"
  cidr_block              = "${cidrsubnet("${var.vpc_cidr}", 8, count.index + 20)}"
  availability_zone       = "${data.aws_availability_zones.available.names[count.index]}"
  map_public_ip_on_launch = true

  tags {
    Name        = "${var.vpc_environment}_${var.subnet_partial_prefix}${count.index + 20}"
    Provisioner = "Terraform"
    Owner       = "${var.owner_email}"
    Environment = "${var.vpc_environment}"
  }
}

resource "null_resource" "subnet_names_from_tags" {
  count = "${var.subnet_count}"

  triggers {
    trig = "${lookup(aws_subnet.usv-subnets-sandbox-test.*.tags[count.index], "Name")}"
  }
}

output "subnet_ids" {
  value = "${aws_subnet.usv-subnets-sandbox-test.*.id}"
}

output "cidr_blocks" {
  value = "${aws_subnet.usv-subnets-sandbox-test.*.cidr_block}"
}

output "subnet_names" {
  value = "${null_resource.subnet_names_from_tags.*.triggers.trig}"
}
