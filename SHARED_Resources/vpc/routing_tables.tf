resource "aws_route_table" "primary_route_table" {
  vpc_id = "${aws_vpc.usv-vpc-sandbox-test.id}"

  tags {
    Name        = "${var.primary_routing_table_name}"
    Owner       = "${var.owner_email}"
    Provisioner = "Terraform"
    Environment = "${var.vpc_environment}"
  }
}

resource "aws_main_route_table_association" "marta" {
  vpc_id         = "${aws_vpc.usv-vpc-sandbox-test.id}"
  route_table_id = "${aws_route_table.primary_route_table.id}"
}

resource "aws_route_table_association" "primary_route_association" {
  count          = "${var.subnet_count}"
  subnet_id      = "${element(aws_subnet.usv-subnets-sandbox-test.*.id, count.index)}"
  route_table_id = "${aws_route_table.primary_route_table.id}"
}

output "primary_route_table_id" {
  value = "${aws_route_table.primary_route_table.id}"
}

output "primary_route_table_association" {
  value = "${aws_route_table_association.primary_route_association.*.id}"
}

output "primary_route_table_association_id" {
  value = "${aws_main_route_table_association.marta.id}"
}
