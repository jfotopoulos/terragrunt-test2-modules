# VPC resource related variables
variable "vpc_cidr" {
  description = "This defines the cidr that the vpc will use. This is usually a /16 cidr."
}

variable "enable_DNS_names" {
  description = "Whether the enable_dns_hostnames option is enabled on the vpc or not."
  default     = true
}

variable "enable_DNS_support" {
  description = "Whether the enable_DNS_hostnames option is enabled on the vpc or not."
  default     = true
}

variable "tenancy" {
  description = "The default tenancy in this VPC"
  default     = "default"
}

variable "vpc_name" {
  description = "The name of the VPC"
}

variable "owner_email" {
  description = "The email of the owner of this resource."
}

variable "vpc_environment" {
  description = "In which environment is this resource located?"
  default     = "sandbox"
}

# Internet Gateways related variables
variable "igw_name" {
  description = "A descriptive name for this VPC Internet Gateway"
  default     = "default"
}

# Subnets related variables
variable "subnet_count" {
  description = "The amount of subnets to be created"
  default     = "1"
}

variable "subnet_partial_prefix" {
  description = "A prefix for the subnet name. The suffix will be the value of subnet_count."
  default     = "default_subnet."
}

# Route table related vars
variable "primary_routing_table_name" {
  description = "This will be used as the name of the primary routing table in this vpc."
  default     = "primary_route_table"
}

variable "vpc_routes_with_primary_default_gw" {
  description = "This is a list with destinations that will be added to the primary routing table and will use the primary GW."
  default     = []
}
